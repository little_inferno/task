import cats.effect.IO

/**
 *  На вход List[IO[String]]
 *  Получить IO[(List[String], List[Throwable]) - результат агрегации выполненых IO и исключений
 */

object Main extends App {

  val colors = List(
    IO.pure("red"),
    IO.raiseError(new RuntimeException("exception1")),
    IO.pure("blue"),
    IO.raiseError(new RuntimeException("exception2")),
    IO.pure("green"),
    IO.raiseError(new RuntimeException("exception3"))
  )

}
