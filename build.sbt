ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.10"

lazy val root = (project in file("."))
  .settings(
    name := "task",
    libraryDependencies ++= Seq(
      "org.typelevel" %% "cats-effect" % "2.5.5",
      "org.typelevel" %% "cats-core" % "2.9.0",
    )
  )
